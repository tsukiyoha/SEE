#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#define NB_TOKEN 5

// prototypes de fonctions
void print_parent(char *line);
void print_cpu_info(void);
void print_mem_info(void);
void print_pid_info(char *pid);

int main(int argc, char **argv)
{
	int pid = 0, opt = 0;
	
	if(argc < 2)
	{
		printf("Usage : %s [-c (cpuinfo)] [-m (memory)] [-p <pid> (pid info)]\n", argv[0]);
		return 1;
	}
	
	while ((opt = getopt(argc, argv, "cmp:")) != -1) {
		switch (opt) {
			case 'c':
				// cpu info
				print_cpu_info();
				break;
			case 'm':
				// mem info
				print_mem_info();
				break;
			case 'p':
				// pid info   
				//pid = atoi(optarg);
				print_pid_info(optarg);
				break;
			default: /* '?' */
				printf("Usage : %s [-c (cpuinfo)] [-m (memory)] [-p <pid> (pid info)]\n", argv[0]);
				return 1;
       }
   }

	return 0;
}

void print_cpu_info(void)
{
	FILE *file;
	size_t len = 0;
    int read = 0;
	char *line, *token[4] = {"model name", "cpu MHz", "cache size", "address sizes"};


	//CUPU  info
	file = fopen("/proc/cpuinfo","r");
	if(file == NULL)
	{
		perror("open cpuinfo");
		exit(1);
	}

	printf("information du CPU : \n");
	while(getline(&line, &len, file) >0 && read <4)
	{
		for(int i = 0; i<4;i++)
		{
			if(strstr(line,token[i]) != NULL)
			{
				printf("%s",line);
				read++;
			}
		}
	}
	read = 0;
	fclose(file);
	printf("\n");
	
	return;
}

void print_mem_info(void)
{
	FILE *file;
	size_t len = 0;
    int read = 0;
	char *line, *tok_mem[2] = {"MemTotal", "MemFree"};


	file = fopen("/proc/meminfo","r");
	if(file == NULL)
	{
		perror("open meminfo");
		exit(1);
	}
	printf("information de la memoire: \n");
	while(getline(&line, &len, file) >0 && read <2)
	{
		for(int i = 0; i<2;i++)
		{
			if(strstr(line,tok_mem[i]) != NULL)
			{
				printf("%s",line);
				read++;
			}
		}
	}
	read = 0;
	fclose(file);
	printf("\n");

}

void print_pid_info(char *pid)
{
	FILE *file;
	size_t len = 0;
    int size = 0;
    char filepath[50] = "/proc/";
	char *line, *token[NB_TOKEN] = {"Tgid", "PPid", "Threads", "Uid", "Gid"};
	
	int val_pid = 0;
	
	val_pid = atoi(pid);
	
	printf("informations sur le PID : \n");
	printf("PID : %d\n",val_pid);

	//Commande
	strcat(filepath,pid);
	strcat(filepath,"/cmdline");
	file = fopen(filepath,"r");
	if(file == NULL)
	{
		perror("open cmdline, pid might not exist...");
		exit(1);
	}

	printf("Commande : ");
	getline(&line, &len, file);
	printf("%s\n", line);
	fclose(file);
	
	// status info
	strcpy(filepath, "/proc/");
	strcat(filepath, pid);
	strcat(filepath, "/status");
	file = fopen(filepath,"r");
	if(file == NULL)
	{
		perror("open status");
		exit(1);
	}

	
	while(getline(&line, &len, file) >0)
	{
		for(int i = 0; i<NB_TOKEN;i++)
		{
			if(strstr(line,token[i]) != NULL)
			{
				printf("%s",line);
				if(i == 1)
				{
					print_parent(line);
				}
			}
		}
	}
	fclose(file);	

	return;
}


void print_parent(char *line)
{
	int val = 0;
	FILE *file_parent;
	char filepath[50] = "/proc/",*c, cmd[100];//*cmd;
	// recherche de chiffre dans la chaine
	line = strpbrk(line,"0123456789");
	// remove the \n
	c = strpbrk(line,"\n");
	*c = '\0';
	
	// constructing file path
	strcat(filepath,line);
	strcat(filepath,"/cmdline");
	file_parent = fopen(filepath,"r");
	if(file_parent == NULL)
	{
		perror("open cmdline, parent pid might not exist...\n");
		return;
	}

	printf("Commande parent : ");
	//getline(&cmd, NULL, file_parent);
	fgets(cmd,100,file_parent);
	printf("%s\n", cmd);
	fclose(file_parent);
	
	return;
}
