#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <string.h>

int main(void)
{
	FILE *file;
	size_t len = 0;
    int read = 0, size = 0;
	char *line, *token[4] = {"model name", "cpu MHz", "cache size", "address sizes"};
	char *tok_mem[2] = {"MemTotal", "MemFree"}, part_name[10];


	//CUPU  info
	file = fopen("/proc/cpuinfo","r");
	if(file == NULL)
	{
		perror("open cpuinfo");
		exit(1);
	}

	printf("information du CPU : \n");
	while(getline(&line, &len, file) >0 && read <4)
	{
		for(int i = 0; i<4;i++)
		{
			if(strstr(line,token[i]) != NULL)
			{
				printf("%s",line);
				read++;
			}
		}
	}
	read = 0;
	fclose(file);
	printf("\n");

	//Memory info
	file = fopen("/proc/meminfo","r");
	if(file == NULL)
	{
		perror("open meminfo");
		exit(1);
	}
	printf("information de la memoire: \n");
	while(getline(&line, &len, file) >0 && read <2)
	{
		for(int i = 0; i<2;i++)
		{
			if(strstr(line,tok_mem[i]) != NULL)
			{
				printf("%s",line);
				read++;
			}
		}
	}
	read = 0;
	fclose(file);
	printf("\n");
	
	//Partions info
	file = fopen("/proc/partitions","r");
        if(file == NULL)
        {
                perror("open partition");
                exit(1);
        }
        printf("information de la memoire: \n");
        while(getline(&line, &len, file) >0 && read <2)
	{
		printf("%s",line);
	}
	fclose(file);
	printf("\n");


	file = fopen("/proc/version","r");
        if(file == NULL)
        {
                perror("open version");
                exit(1);
        }
        printf("information de version: \n");
        getline(&line, &len, file); // strtok ?
	
	printf("%s", line);

}
