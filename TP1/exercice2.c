#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#define NB_TOKEN 5

// prototypes de fonctions
void print_parent(char *line);

int main(int argc, char **argv)
{
	FILE *file;
	size_t len = 0;
    int size = 0;
    char filepath[50] = "/proc/";
	char *line, *token[NB_TOKEN] = {"Tgid", "PPid", "Threads", "Uid", "Gid"};
	
	int pid = 0;
	
	if(argc < 2)
	{
		printf("Usage : %s pid\n", argv[0]);
		return(1);
	}

	pid = atoi(argv[1]);
	
	printf("PID : %d\n",pid);

	//Commande
	strcat(filepath,argv[1]);
	strcat(filepath,"/cmdline");
	file = fopen(filepath,"r");
	if(file == NULL)
	{
		perror("open cmdline, pid might not exist...");
		exit(1);
	}

	printf("Commande : ");
	getline(&line, &len, file);
	printf("%s\n", line);
	fclose(file);
	
	// status info
	strcpy(filepath, "/proc/");
	strcat(filepath, argv[1]);
	strcat(filepath, "/status");
	file = fopen(filepath,"r");
	if(file == NULL)
	{
		perror("open status");
		exit(1);
	}

	
	while(getline(&line, &len, file) >0)
	{
		for(int i = 0; i<NB_TOKEN;i++)
		{
			if(strstr(line,token[i]) != NULL)
			{
				printf("%s",line);
				if(i == 1)
				{
					print_parent(line);
				}
			}
		}
	}
	fclose(file);	

	return 0;
}


void print_parent(char *line)
{
	int val = 0;
	FILE *file_parent;
	char filepath[50] = "/proc/",*c, cmd[100];//*cmd;
	// recherche de chiffre dans la chaine
	line = strpbrk(line,"0123456789");
	// remove the \n
	c = strpbrk(line,"\n");
	*c = '\0';
	
	// constructing file path
	strcat(filepath,line);
	strcat(filepath,"/cmdline");
	file_parent = fopen(filepath,"r");
	if(file_parent == NULL)
	{
		perror("open cmdline, parent pid might not exist...\n");
		return;
	}

	printf("Commande parent : ");
	//getline(&cmd, NULL, file_parent);
	fgets(cmd,100,file_parent);
	printf("%s\n", cmd);
	fclose(file_parent);
	
	return;
}

