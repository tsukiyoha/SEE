#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <sched.h>
#include <math.h>

#define NB_PERIODES 100
#define PERIODE_100MS 100000000
#define PERIODE_50MS 50000000
#define PERIODE_500MS 500000000

timer_t timerid;
struct itimerspec its;
int counter_stop = 0;
long int timer_values[NB_PERIODES], diff_times[NB_PERIODES-1];


static void sig_handler(int sig, siginfo_t *si, void *uc);

int signal_launch(int periode);

void analisys(int periode);

int main(void)
{
	long int temp;
	int err = 0;

	// Periode 100ms
	err = signal_launch(PERIODE_100MS);
	if(err!=0)
	{
		perror("periode 100ms");
		return err;
	}

	analisys(PERIODE_100MS);

	// Periode 50ms
	err = signal_launch(PERIODE_50MS);
	if(err!=0)
	{
		perror("periode 50ms");
		return err;
	}

	analisys(PERIODE_50MS);

	// Periode 500ms
	err = signal_launch(PERIODE_500MS);
	if(err!=0)
	{
		perror("periode 500ms");
		return err;
	}

	analisys(PERIODE_500MS);


	return 0;
}

static void sig_handler(int sig, siginfo_t *si, void *uc)
{
	struct itimerspec time;
	timer_gettime(timerid, &time);
	if(counter_stop<NB_PERIODES)
	{
		timer_values[counter_stop] = time.it_value.tv_nsec;
	}
	counter_stop++;
}

int signal_launch(int periode)
{
	struct sigevent sev;
	struct itimerspec its;
	struct sigaction sa;
	
	// create signal
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = sig_handler;
	if (sigaction(SIGRTMIN, &sa, NULL) == -1)
	{
		perror("sigaction");
		return 1;
	}
	
	// creating timer
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGRTMIN;
	sev.sigev_value.sival_ptr = &timerid;
	if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1)
	{
		perror("timer_create");
		return 2;
	}

	// starting timer
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = periode;
	its.it_interval.tv_sec = its.it_value.tv_sec;
	its.it_interval.tv_nsec = its.it_value.tv_nsec;

	if (timer_settime(timerid, 0, &its, NULL) == -1)
	{
		perror("timer_settime");
		return 3;
	}

	// letting work get done
	while(1)
	{
		sleep(1);
		if(counter_stop >= NB_PERIODES)
		{
			its.it_value.tv_nsec = 0;
			its.it_interval.tv_nsec = its.it_value.tv_nsec;
			if (timer_settime(timerid, 0, &its, NULL) == -1)
			{
				perror("timer_settime disarm");
				return 3;
			}
			break;
		}
	}
}

void analisys(int periode)
{
	int temp = 0;
	float moyenne = 0.0,variance = 0.0, ecart_type; 

	// moyenne
	for(int i = 0; i<(NB_PERIODES-1); i++)
	{
		temp = timer_values[i+1]-timer_values[i];
		diff_times[i] = (temp>0)? temp: -temp;
		moyenne += diff_times[i];
	}
	moyenne = moyenne / (NB_PERIODES-1);

	// variance
	for(int i = 0; i<(NB_PERIODES-1); i++)
	{
		variance += (diff_times[i]-moyenne)*(diff_times[i]-moyenne);
	}
	variance = variance / (NB_PERIODES-1);

	// ecart-type
	ecart_type = sqrtf(variance);
	switch (periode)
	{
		case PERIODE_100MS:
			printf("\nPour la période de 100ms\n");
			break;
		case PERIODE_50MS:
			printf("\nPour la période de 500ms\n");
			break;
		case PERIODE_500MS:
			printf("\nPour la période de 500ms\n");
			break;
	
	default:
		printf("période non reconnue");
		break;
	}
	
	printf("Moyenne : %3f, Ecart type : %3f\n", moyenne, ecart_type);
	
	return;

}