#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define LIM 20

void main(void)
{
    int val = 0;
    while(1)
    {
        val = random();
        if(val<LIM)
        {
            printf("I'm going to sleep\n");
            sleep(2);
            printf("I'm back from sleep\n");
        }
    }
    return;
}